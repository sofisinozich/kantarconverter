<!DOCTYPE html>
<html>
    <head>
        <title>PARC Spreadsheet Converter</title>
        <link rel="icon" href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🔄</text></svg>">
        <style>
            #content {
                font-family: sans-serif;
                position: absolute;
                left: 50%;
                transform: translate(-50%);
                line-height: 2em;
            }

            input[name="document"] {
                width: 100%;
            }

            input[name="saveto"] {
                width: 100%;
            }

            input[name="sheetnum"] {
                width: 5ch;
            }

            input[name="headerrange"] {
                width: 10ch;
            }

            input[name="lookfor"] {
                width: 15ch;
            }

            input[name="columnin"] {
                width: 3ch;
            }

            .hint {
                font-size: 75%;
                color: darkgrey;
            }

            #progress {
                font-style: italic;
                display: none;
            }

            h4 {
                margin-bottom: .5em;
                border-top: solid 1px black;
                border-bottom: solid 1px black;
            }

            .disabled {
                color: darkgray;
            }

            .disabled input {
                color: darkgray;
            }

            fieldset {
                border: 0;
            }
        </style>

        <script type="text/javascript">
            function inprog() {
                // Check that everything is filled
                filled = true;
                for (i of document.querySelectorAll("input")) {
                    if ((i.type == "text" || i.type == "file") && i.value == "") {
                        filled = false;
                    }
                }

                if (filled) {
                    document.getElementById("progress").style.display = 'block';
                    startTime = new Date().toLocaleString();
                    document.getElementById("progress").innerText = "Conversion started on "+document.querySelector("input[type='file']").value.split(/[/|\\]/).pop()+" ("+startTime+")";
                }
            }

            function endprog() {
                document.getElementById("progress").style.display = 'none';
                document.getElementById("progress").innerText = "Working on it...";
            }

            function defaults() {
                switch(document.querySelector("input[name='preset']:checked").id) {
                    case 'custom': 
                        document.getElementById("qdetails").className = "";
                        for (let input of document.getElementById("qdetails").getElementsByTagName("input")) {input.readOnly = false;};
                        break;
                    case 'marist':                 
                        document.getElementById("qdetails").className = "disabled";
                        document.getElementById("columnin").value = "A";
                        document.getElementById("lookfor").value = "([A-Z0-9]+)\\. ";
                        document.getElementById("changeto").value = "\\1. ";
                        document.getElementById("at").checked = true;
                        document.getElementById("below").checked = false;
                        for (let input of document.getElementById("qdetails").getElementsByTagName("input")) {input.readOnly = true;};
                        break;
                    case 'kantar':                 
                        document.getElementById("qdetails").className = "disabled";
                        document.getElementById("columnin").value = "A";
                        document.getElementById("lookfor").value = "Table ([0-9]+)";
                        document.getElementById("changeto").value = "T.\\1 ";
                        document.getElementById("at").checked = false;
                        document.getElementById("below").checked = true;
                        for (let input of document.getElementById("qdetails").getElementsByTagName("input")) {input.readOnly = true;};
                        break;
                }
            }
        </script>
    </head>

    <body>
        <div id="content">
            <h1>PARC Spreadsheet Converter - BETA</h1>

            <p>This is a beta version of the converter. Please report any issues to your PARC representative, with a description of what you were trying to do and, if possible, the file you were attempting to convert. Thank you!</p>

            <p>Need help? Check out a brief <a href="/guide">guide</a> to the converter's functions.</p>
            <form action="/upload" method="post" enctype="multipart/form-data">
                <h4>File details</h4>
                <!-- Path to file for processing: <input name="document" type="text" value="C:/Users/Sofi/Dropbox (Langer Research)/Langer Research Team Folder/PARC/Demos-Troubleshooting-Uploading/Kantar Consulting/KantarConverter/Testing/Example of New Table Format.xlsx" required /><br> -->
                File: <input name="document" type="file" id="file" required /><br>
                Sheet number: <input name="sheetnum"  type="number" min="0" value="0" required /> <span class="hint">Numbering starts from 0.</span><br>
                Header range: <input name="headerrange" type="text" value = "C5:AR7" required /> <span class="hint">Should be in the form A3:B8</span><br>
                <!-- Save to (optional): <input name="saveto" type="text" value="C:/Users/Sofi/Dropbox (Langer Research)/Langer Research Team Folder/PARC/Demos-Troubleshooting-Uploading/Kantar Consulting/KantarConverter/Testing/Example of New Table Format test.xlsx"/><br> -->
                
                <h4>Question details</h4>
                Fill in these details OR select a preset with defaults for your organization.<br>
                <fieldset>
                    <input type="radio" name="preset" id="custom" onchange="defaults()" checked> <label for="custom">Custom</label>
                    <input type="radio" name="preset" id="kantar" onchange="defaults()"> <label for="kantar">Kantar Consulting</label>
                    <input type="radio" name="preset" id="marist" onchange="defaults()"> <label for="marist">Marist Poll</label>
                </fieldset>
                <div id ="qdetails">
                    Question column: <input name="columnin" id="columnin" type="text" value="A" required /><br>
                    Question format: <input name="lookfor" id="lookfor" type="text" value="[A-Z0-9]+: " required /> <span class="hint">Use <a href="https://web.mit.edu/hackl/www/lab/turkshop/slides/regex-cheatsheet.pdf">regular expressions</a>.</span><br>
                    Replace question with: <input name="changeto" id="changeto" type="text" value="" required /> <span class="hint">You can use regex substitution.</span><br>
                    Question text is: <input type="radio" name="qwhere" value="at" id="at" checked> <label for="at">Level with first response option</label> <input type="radio" name="qwhere" value="below" id="below" selected> <label for="below">Above response options</label><br>
                </div><br>
                <input value="Convert" type="submit" onclick="inprog()"/> <input type="reset" onclick="endprog()">
            </form>

            <div id="progress">Working on it...</div>
        </div>
    </body>
</html>