from bottle import Bottle, route, run, get, post, request, template, static_file
from pathlib import Path
import os
from converter import *
from openpyxl import load_workbook

app = Bottle()

@app.route('/')
def start():
    return template('front.tpl')

@app.route('/guide')
def guide():
    return static_file('guide.html',root='')

@app.route('/upload', method='POST')
def upload():
    document = request.files.get('document')
    docname, docext = os.path.splitext(document.filename)
    if ".xlsx" not in docext:
        return template('result.tpl',resultmessage='File type should be .xlsx',redirectmessage='Try again.')
    else:
        saveto = request.forms.get('saveto')
        if saveto is None:
            saveto = docname+"_edited"
        document = openpyxl.load_workbook(request.files.get('document').file)
        sheetnum = int(request.forms.get('sheetnum'))
        headerRange = request.forms.get('headerrange')
        lookfor = request.forms.get('lookfor')
        qwhere = request.forms.get('qwhere')
        columnin = request.forms.get('columnin')
        changeto = request.forms.get('changeto')

        # result, redirect, filepath = createFile(document=document,sheetnum=sheetnum,headerRange=headerRange,saveto=saveto,lookfor=lookfor,qwhere=qwhere,columnin=columnin,changeto=changeto)
        # return template('result.tpl',resultmessage=result,redirectmessage=redirect,filepath=filepath)
        result, value, redirect = createFile(document=document,sheetnum=sheetnum,headerRange=headerRange,saveto=saveto,lookfor=lookfor,qwhere=qwhere,columnin=columnin,changeto=changeto)
        if result == 1:
            head, tail = os.path.split(value)
            return static_file(filename=tail, root=head, download=saveto+".xlsx")
        else:
            return template('result.tpl', resultmessage=result, redirectmesage=redirect)

if __name__ == '__main__':
    app.run()