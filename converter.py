import openpyxl
from copy import copy
from openpyxl import load_workbook, styles
import string
import time
import traceback
from re import sub, search
from tempfile import NamedTemporaryFile
import sys
import os

def col2num(col):
    """Convert alpha indexes to numeric"""
    num = 0
    try:
        for c in col:
            if c in string.ascii_letters:
                num = num * 26 + (ord(c.upper()) - ord('A')) + 1
    except:
        num = col
    return num

# This is NOT an all-purpose copying function. Please only use for moving the header around.
def copyRange(dataSheet,copyRange,pasteStartRow,pasteStartCol):
    """Helper for adding the header to each question"""
    for row in range(len(dataSheet[copyRange])):
        for col in range(len(dataSheet[copyRange][row])):
            targetCell = dataSheet.cell(row=pasteStartRow+row,column=pasteStartCol+col)
            sourceCell = dataSheet[copyRange][row][col]
            targetCell.value = sourceCell.value
            targetCell._style = copy(sourceCell._style)

    offsetY = pasteStartRow - dataSheet[copyRange][0][0].row 
    offsetX = pasteStartCol - col2num(dataSheet[copyRange][0][0].column)

    for m in dataSheet.merged_cells.ranges:
        mergedRange = dataSheet[m.coord]
        for row in dataSheet[copyRange]:
            if set(mergedRange[0]) <= set(row):
                firstCell = mergedRange[0][0]
                lastCell = mergedRange[len(mergedRange)-1][len(mergedRange[len(mergedRange)-1])-1]
                dataSheet.merge_cells(start_row = firstCell.row+offsetY,start_column=col2num(firstCell.column)+offsetX,end_row=lastCell.row+offsetY,end_column=col2num(lastCell.column)+offsetX)
            
def loopReplace(dataSheet,columnLetter,findString,replaceString,headerRange,qwhere):
    """Loops through first column looking for strings and replaces them with... other strings. Notes at which row those strings were found. Expands to make room for header."""
    headerHeight = dataSheet[headerRange][-1][0].row - dataSheet[headerRange][0][0].row+1
    foundLocs = []
    for cell in dataSheet[columnLetter]:
        try:
            if bool(search(findString,cell.value)) == True:
                cell.value=sub(findString,replaceString,cell.value)
                foundLocs.append(cell.row+1)
                if qwhere == "below":
                    # Combine the cells (since question text is always in next row)
                    cell.value=cell.value + cell.offset(1,0).value
                    # Remove text in the next row
                    cell.offset(1,0).value = ""
            if qwhere == "below":
                if "Base" in cell.value and cell.row-foundLocs[-1]<headerHeight+1:
                    for j in range(headerHeight-(cell.row-foundLocs[-1])):
                        dataSheet.insert_rows(cell.row)
            if qwhere == "at":
                if cell.row-foundLocs[-1]<headerHeight+1:
                    for j in range(headerHeight-(cell.row-foundLocs[-1])+2):
                        dataSheet.insert_rows(cell.row)
                # Move the cell to the top of the new values (assumes header does not encroach onto first column and that question is in the next row)
                cell.offset(-(headerHeight+1),0).value = cell.value
                cell.offset(-(headerHeight+1),0).style = cell.style
                cell.value = ""
                foundLocs[-1] = foundLocs[-1]+1
        except:
            continue
    return foundLocs

def createFile(document,sheetnum,headerRange,saveto,lookfor,qwhere,columnin,changeto):
    start = time.process_time()
    """Generate new file and save back down."""
    wkbk = document
    dataSheet = wkbk.copy_worksheet(wkbk.worksheets[sheetnum])
    wkbk._sheets.insert(0,wkbk._sheets.pop())
    dataSheet.title = "PARC"

    questionLocs = loopReplace(dataSheet,columnin,lookfor,changeto,headerRange,qwhere)

    for q in questionLocs:
        if qwhere == "at":
            q = q+1
        copyRange(dataSheet,headerRange,q,dataSheet[headerRange][0][0].column)

    try:
        tmp = NamedTemporaryFile(delete=False)
        wkbk.save(tmp.name)
        return 1,tmp.name,None

        # with NamedTemporaryFile(delete=False) as tmp:
        #     wkbk.save(tmp.name)
        #     head, tail = os.path.split(tmp.name)
        #     tmp.close()
            
        # end = time.process_time()-start
        # return 'Success!\nSaved to: '+saveto+"\nThis took "+str(round(end,2))+" seconds.","Another?"
    except:
        return 0,'Something went wrong: \n'+traceback.format_exc(),"Try again.",None